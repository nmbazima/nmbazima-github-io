---
layout: page
title: 
permalink: /about/
---

Information Systems Developer @ <a href="www.educoreservices.com">EducoreServices</a>  
(Python, R, SQL, Other stuff)

* Data Science from HarvardX University.
* Network Engineering from NIIT University.


## More Information
<a href="https://nmbazima.github.io">Github Profile</a>  

## Contact me

[nmbazima@gmail.com](mailto:nmbazima@gmail.com)
